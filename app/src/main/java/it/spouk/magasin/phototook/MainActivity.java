package it.spouk.magasin.phototook;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import it.spouk.magasin.FileUploadService;
import it.spouk.magasin.ServiceGenerator;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private Button _takepicturebutton;
    private ImageView _view;
    private Uri _file;
    private File _fileNewSdk;
    private static TextView _filePath;
    private Button _deleteCurrentSelectionButton;
    private ProgressBar _progBar;
    private Button _sendPicture;
    private static final String AUTHORITY = "com.commonsware.android.cp.v4file";
    private CheckedTextView _okdisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("onCreate", "It real!");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _takepicturebutton = (Button) findViewById(R.id.takepicBtn);
        _deleteCurrentSelectionButton = (Button) findViewById(R.id.deleteContentBtn);
        _view = (ImageView) findViewById(R.id.viewPicImg);
        _filePath = (TextView) findViewById(R.id.FileLocationTextView);
        _progBar = (ProgressBar) findViewById(R.id.progressBar);
        _sendPicture = (Button) findViewById(R.id.sendPictureBtn);
        _okdisplay = (CheckedTextView) findViewById(R.id.sendOkCheckedTextView);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            _takepicturebutton.setEnabled(true);
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d("PERMISSION", "It's OK!");
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                _takepicturebutton.setEnabled(true);
            }
        }
    }

    public void takePicture(View view) {
        Intent intent;
        _file = null;
        _fileNewSdk = null;
        _filePath.setText("");
        _view.setImageURI(null);
        _deleteCurrentSelectionButton.setVisibility(View.INVISIBLE);
        _sendPicture.setVisibility(View.INVISIBLE);
        _progBar.setVisibility(View.INVISIBLE);
        _okdisplay.setVisibility(View.INVISIBLE);
//        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.KITKAT) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.N) {
            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            _file = Uri.fromFile(getOutputMediaFile());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, _file);
            startActivityForResult(intent, 100);
        } else {
            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            _file = Uri.fromFile(getOutputMediaFile());
//            intent = new Intent(intentValue, uriFromFile);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            _fileNewSdk = getOutputMediaFile();
            String intentValue = Intent.ACTION_VIEW;
            if (this == null || AUTHORITY == null || _fileNewSdk == null) {
                Log.e("NewSDK", "Error, a variable/value is null");
            }
            if (getPackageManager() == null) {
                Log.e("PM", "Error, no packagemanager");
            }
            Uri uriFromFile = null;
            if (Build.VERSION_CODES.M > Build.VERSION.SDK_INT) {
                uriFromFile = FileProvider.getUriForFile(this, AUTHORITY, _fileNewSdk);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, _file);
            } else {
                uriFromFile = FileProvider.getUriForFile(getApplicationContext(), "it.spouk.magasin.fileProvider", _fileNewSdk);
//                getApplicationContext().grantUriPermission("it.spouk.magasin.phototook", uriFromFile, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            if (intentValue == null || uriFromFile  == null) {
                Log.e("IntentValue", "Error, intentValue OR uriFromFile is null");
            }
            startActivityForResult(intent, 100);

        }
    }

    public void deleteImageSelected(View view) {
        _file = null;
        _fileNewSdk = null;
        _filePath.setText("");
        _view.setImageURI(null);
        _deleteCurrentSelectionButton.setVisibility(View.INVISIBLE);
        _sendPicture.setVisibility(View.INVISIBLE);
        _progBar.setVisibility(View.INVISIBLE);
        _okdisplay.setVisibility(View.INVISIBLE);
    }

    /*
        For uploading the choosen image
     */
    public void uploadImage(View v) throws Exception {
        _progBar.setVisibility(View.VISIBLE);
        // create upload service client
        FileUploadService service =
                ServiceGenerator.createService(FileUploadService.class);

        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
//        FileInputStream fileInputStream = new FileInputStream(_file.getPath());
        File file = null;
        if (_file != null) {
            file = new File(_file.getPath());
        } else {
            if (_fileNewSdk != null) {
                Log.e("DEBUG", "fileNewSdk : " + _fileNewSdk.getPath());
                _file = Uri.parse(String.valueOf(_fileNewSdk.toURI()));
                file = new File(_file.getPath());
            } else {
                _file = Uri.parse(_filePath.getText().toString());
                Log.e("DEBUG2", "File : " + _filePath.getText().toString() + " to " + Uri.parse(_filePath.getText().toString()));
                file = new File(_file.getPath());
            }
        }

//        if (_file == null) {
//            _file = Uri.parse(String.valueOf(_fileNewSdk.toURI()));
//        }

        Log.e("PRE SEND", "File : " + file.getPath().toString());
        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(file.toString()),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("picture", file.getName(), requestFile);

        // add another part within the multipart request
        String descriptionString = "hello, this is description speaking";
        RequestBody description =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, descriptionString);

        // finally, execute the request
        Call<ResponseBody> call = service.upload(description, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {
                Log.v("Upload", "success : "+ response.toString());
                // Display OK!
                _progBar.setVisibility(View.INVISIBLE);
                _okdisplay.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    /*
        For searching in the gallery to find a pre-saved image
     */
    public static final int RESULT_LOAD_IMG = 100;
    public void searchInGallery(View view) {
        _file = null;
        _fileNewSdk = null;
        _filePath.setText("");
        _view.setImageURI(null);
        _deleteCurrentSelectionButton.setVisibility(View.INVISIBLE);
        _sendPicture.setVisibility(View.INVISIBLE);
        _progBar.setVisibility(View.INVISIBLE);
        _okdisplay.setVisibility(View.INVISIBLE);
        Log.d("GALSEARCH", "It begin");
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Log.d("GALSEARCH", "It's OK!");
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }

    @Override
    protected void onActivityResult(int RequestCode, int ResultCode, Intent Data) {
        Log.d("RESULT 100", "Gonna check!");
        if (RequestCode == 100) {
            if (ResultCode == RESULT_OK) {
                if (_file != null) {
                    if (_filePath != null) {
                        Log.d("RESULT 100", "It's OK! : " + _filePath + " || " + _file.getPath());
                        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                            _filePath.setText(_file.getPath());
                        }
                        _view.setImageURI(_file);
                    } else {
                        Log.d("RESULT 100", "IT's about to get OK! " + _file.getPath());
                        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                            _filePath.setText(_file.getPath());
                        }
                    }
                } else {
                    if (_fileNewSdk != null) {
                        Log.d("RESULT 100", "It's OK! : " + _fileNewSdk.getPath());
                        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                            _filePath.setText(_fileNewSdk.getPath());
                        }
                        _view.setImageURI(Uri.parse(_fileNewSdk.toString()));
                    } else if (Data != null) {
                        Log.d("TEst0" , "Intent = " + Data.toString());
                        _filePath.setText(Data.getData().toString());
                        Log.d("TEst0" , "_filePath = " + _filePath.getText());
                        _view.setImageURI(Uri.parse(Data.toUri(0)));
                        Uri tmpUri = MediaStore.Images.Media.getContentUri("external");
                        //getContentResolver().acquireProvider("media")


                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        Cursor cursor = getContentResolver().query(Data.getData(), filePathColumn, null, null, null);
                        assert cursor != null;
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String picturePath = cursor.getString(columnIndex);
                        cursor.close();
                        String fileNameSegments[] = picturePath.split("/");
                        String filename = fileNameSegments[fileNameSegments.length - 1];

                        _file = Uri.parse(picturePath);


//                        _file = tmpUri;
//                        _file = Uri.parse(String.valueOf(Uri.parse(String.valueOf(Data.getData()))));
                        _deleteCurrentSelectionButton.setVisibility(View.VISIBLE);
                        _sendPicture.setVisibility(View.VISIBLE);
                    } else {
                        Log.e("TEst0", "Error intent is null");
                    }
                }
                _deleteCurrentSelectionButton.setVisibility(View.VISIBLE);
                _sendPicture.setVisibility(View.VISIBLE);
            } else if (ResultCode == RESULT_CANCELED) {
                _file = null;
                _filePath.setText("");
                _fileNewSdk = null;
            }
        } else {
            if (Data != null) {
                Log.d("TEst1" , "Intent = " + Data.toString());
                _filePath.setText(Data.getData().toString());
                Log.d("TEst1" , "_filePath = " + _filePath);
                _view.setImageURI(Uri.parse(Data.toUri(0)));
                _deleteCurrentSelectionButton.setVisibility(View.VISIBLE);
                _sendPicture.setVisibility(View.VISIBLE);
            } else {
                Log.e("TEst1", "Error intent is null");
            }
        }
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                android.os.Environment.DIRECTORY_PICTURES), "PhotoTook");
        Log.e("OutputFile", "getOutputMediaFile : " + mediaStorageDir.toString());
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.e("OutputFile", "Error getting storage.");
                return null;
            }
            Log.e("OutputFile", "Storage don't exists but created.");
        }
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileLoc = mediaStorageDir.getPath() + File.separator + "IMG_" + timestamp + ".jpg";
        _filePath.setText(fileLoc);
        return new File(fileLoc);
    }

}



